package com.leanxcale;

import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import org.apache.avro.LogicalTypes;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.SerializationException;

public class KafkaAvroNYCBikesProducer {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSS");

    public static void main (String[] args) throws IOException {

        Properties props = new Properties(); // Create producer configuration
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092"); // Set Kafka server ip:port
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                io.confluent.kafka.serializers.KafkaAvroSerializer.class); // Set record key serializer to Avro
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                io.confluent.kafka.serializers.KafkaAvroSerializer.class); // Set record value serializer to Avro
        props.put("schema.registry.url", "http://localhost:8081");
        KafkaProducer producer = new KafkaProducer(props); // Create producer

        // Create schema objects. We need an chema for the record key and another schema for
        // the record value
        Schema tsType = LogicalTypes.timestampMillis().addToSchema(Schema.create(Schema.Type.LONG));
        // Define the key schema as only one field of Integer type called "id"
        Schema keySchema = SchemaBuilder.record("myKey").namespace("com.leanxcale").fields()
                .name("id").type().intType().noDefault().endRecord();
        // Define the value schema with the rest of the fields defined as columns in the csv file
        Schema valueSchema = SchemaBuilder.record("myValue").namespace("com.leanxcale").fields()
                .name("trip_duration_secs").type().intType().noDefault()
                .name("start_time").type(tsType).noDefault()
                .name("stop_time").type(tsType).noDefault()
                .name("start_station_id").type().intType().noDefault()
                .name("start_station_name").type().stringType().noDefault()
                .name("start_station_latitude").type().doubleType().noDefault()
                .name("start_station_longitude").type().doubleType().noDefault()
                .name("end_station_id").type().intType().noDefault()
                .name("end_station_name").type().stringType().noDefault()
                .name("end_station_latitude").type().doubleType().noDefault()
                .name("end_station_longitude").type().doubleType().noDefault()
                .name("bike_id").type().intType().noDefault()
                .name("user_type").type().stringType().noDefault()
                .name("user_birth_year").type().intType().noDefault()
                .name("user_gender").type().intType().noDefault()
                .endRecord();

        // Open and read csv
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("202001-citibike-tripdata.csv");
        try (CSVReader csvReader = new CSVReader(new BufferedReader(new InputStreamReader(in)))) {
            String[] values;
            int id = 1; // Id will be the PK of the destination table and it's an autoincrement field
            csvReader.readNext(); // Skip header
            while ((values = csvReader.readNext()) != null) {
                // Generate record key
                GenericRecord avroKeyRecord = new GenericData.Record(keySchema); // Create new record following key schema
                avroKeyRecord.put("id", id); // Put autoincrement value
                GenericRecord avroValueRecord = new GenericData.Record(valueSchema); // Create new record following value schema
                // Put values read from the csv file
                avroValueRecord.put("trip_duration_secs", Integer.parseInt(values[0]));
                LocalDateTime dateTime = LocalDateTime.parse(values[1], formatter);
                avroValueRecord.put("start_time", Timestamp.valueOf(dateTime).getTime());
                dateTime = LocalDateTime.parse(values[2], formatter);
                avroValueRecord.put("stop_time", Timestamp.valueOf(dateTime).getTime());
                avroValueRecord.put("start_station_id", Integer.parseInt(values[3]));
                avroValueRecord.put("start_station_name",values[4]);
                avroValueRecord.put("start_station_latitude",Double.parseDouble(values[5]));
                avroValueRecord.put("start_station_longitude",Double.parseDouble(values[6]));
                avroValueRecord.put("end_station_id", Integer.parseInt(values[7]));
                avroValueRecord.put("end_station_name",values[8]);
                avroValueRecord.put("end_station_latitude",Double.parseDouble(values[9]));
                avroValueRecord.put("end_station_longitude",Double.parseDouble(values[10]));
                avroValueRecord.put("bike_id", Integer.parseInt(values[11]));
                avroValueRecord.put("user_type",values[12]);
                avroValueRecord.put("user_birth_year",Integer.parseInt(values[13]));
                avroValueRecord.put("user_gender",Integer.parseInt(values[14]));

                // Create a new producer record for topic: "mytopic", key: created key crecord, value: created value record
                ProducerRecord<Object, Object> record = new ProducerRecord<>("mytopic", avroKeyRecord, avroValueRecord);
                try {
                    RecordMetadata res = (RecordMetadata)producer.send(record).get(); // Send the record to Kafka server
                } catch(SerializationException | InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                id++; // Increment id field (PK)
            }
        }
        // Flush the producer to ensure it has all been written to Kafka and finally close it
        finally {
            producer.flush();
            producer.close();
        }

    }
}
